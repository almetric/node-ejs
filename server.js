// load the things we need
var express = require('express');
var app = express();
var path = require('path');

var bodyparser = require('body-parser')

app.use(bodyparser.json());
// set the view engine to ejs
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'))
app.use(bodyparser.urlencoded({ extended: true }));

// use res.render to load up an ejs view file

// index page 
app.get('/', function (req, res) {
	let forminfo = [{
		id: 1,
		name: 'rahul',
		mail: '12',
		phone: 444
	}, {
		id: 2,
		name: 'rahul',
		mail: '12ee',
		phone: 4433334
	}]
	res.render('pages/index', { forminfo });
});

// about page 
app.get('/about', function (req, res) {
	res.render('pages/about');
});

app.post('/after/submit', function (req, res) {
	// res.render('pages/about');
	console.log("=-=--=reqies-0- ", req.body);
	let forminfo = [{
		name: 'rahul',
		mail: '12',
		phone: 444
	}, {
		name: 'rahul',
		mail: '12ee',
		phone: 4433334
	}]
	res.render('pages/index', { forminfo });
});


app.listen(8080);
console.log('8080 is the magic port');